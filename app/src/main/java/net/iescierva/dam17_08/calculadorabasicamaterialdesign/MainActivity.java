package net.iescierva.dam17_08.calculadorabasicamaterialdesign;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public EditText entrada;
    public double num1, num2, resultado;
    int operacion;
    boolean punto=false;
    boolean num1_ocup=false;
    public Button botonD;
    boolean porcent = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Te vigilamos", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        entrada = (EditText) findViewById(R.id.etActual);
        Button btnDecimal = (Button) findViewById(R.id.btnDecimal);
        botonD = btnDecimal;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void clearEverything(View v){

        entrada.setText("");
        num1 = 0;
        num2 = 0;
        resultado = 0;

        num1_ocup=false;
        punto=false;
        porcent = false;

    }

    public void clear(View v){

        if (!entrada.getText().toString().equals("")){
            entrada.setText("");
        }
        punto=false;
    }

    public void btn(View v){

        String numero=entrada.getText().toString();
        numero += "0";
        entrada.setText(numero);

    }

    public void btn1(View v){

        String numero=entrada.getText().toString();
        numero += "1";
        entrada.setText(numero);

    }

    public void btn2(View v){

        String numero=entrada.getText().toString();
        numero += "2";
        entrada.setText(numero);

    }

    public void btn3(View v){

        String numero=entrada.getText().toString();
        numero += "3";
        entrada.setText(numero);

    }

    public void btn4(View v){

        String numero=entrada.getText().toString();
        numero += "4";
        entrada.setText(numero);

    }

    public void btn5(View v){

        String numero=entrada.getText().toString();
        numero += "5";
        entrada.setText(numero);

    }

    public void btn6(View v){

        String numero=entrada.getText().toString();
        numero += "6";
        entrada.setText(numero);

    }

    public void btn7(View v){

        String numero=entrada.getText().toString();
        numero += "7";
        entrada.setText(numero);

    }

    public void btn8(View v){

        String numero=entrada.getText().toString();
        numero += "8";
        entrada.setText(numero);

    }

    public void btn9(View v){

        String numero=entrada.getText().toString();
        numero += "9";
        entrada.setText(numero);

    }

    public void btnPunto(View v) {

        String cadena = entrada.getText().toString();
        char aBuscar = '.';
        char[] vector = cadena.toCharArray();
        char caracter;

        for (int i = 0; i < cadena.length(); i++) {

            caracter = vector[i];
            if (aBuscar == caracter) {
                punto = true;
                botonD.setEnabled(false);
            }
            else if (punto == false) {
                punto = false;
                botonD.setEnabled(true);
            }
        }
        if (punto == false) {
            String numero = entrada.getText().toString();
            numero += ".";
            entrada.setText(numero);
        }
    }

    public void suma(View v){

        if (num1_ocup==false) {
            try {

                if (porcent == false) {

                    String aux = entrada.getText().toString();
                    num1 = Double.parseDouble(aux);
                    punto = false;
                    num1_ocup = true;
                }
            } catch (NumberFormatException nfe) {
            }
        }
        else{
            try {
                String aux1 = entrada.getText().toString();
                num2 = Double.parseDouble(aux1);

                if (porcent == true){

                    num2=num1*(num2/100);

                }

                switch (operacion) {

                    case 1:
                        num1 = num1 + num2;
                        punto=false;
                        break;
                    case 2:
                        num1 = num1 - num2;
                        punto=false;
                        break;
                    case 3:
                        num1 = num1 * num2;
                        punto=false;
                        break;
                    case 4:
                        if (num2==Double.NaN) {
                            entrada.setText("Error");
                        }
                        else{
                            resultado=num1/num2;
                            punto=false;
                        }
                        break;

                }
            } catch (Exception e) {
                Toast.makeText(this, "Se ha producido un error inesperado.", Toast.LENGTH_SHORT);
            }
        }
        entrada.setText("");
        operacion=1;
    }

    public void resta(View v){

        if (num1_ocup==false) {
            try {

                if (porcent == false) {

                    String aux = entrada.getText().toString();
                    num1 = Double.parseDouble(aux);
                    punto = false;
                    num1_ocup=true;

                }

            } catch (NumberFormatException nfe) {
            }
        }
        else {
            try {
                String aux1 = entrada.getText().toString();
                num2 = Double.parseDouble(aux1);


                if (porcent == true){

                    num2=num1*(num2/100);

                }

                switch (operacion) {

                    case 1:
                        num1 = num1 + num2;
                        punto=false;
                        break;
                    case 2:
                        num1 = num1 - num2;
                        punto=false;
                        break;
                    case 3:
                        num1 = num1 * num2;
                        punto=false;
                        break;
                    case 4:
                        if (num2==Double.NaN) {
                            entrada.setText("Error");
                        }
                        else{
                            resultado=num1/num2;
                            punto=false;
                        }
                        break;

                }
            } catch (Exception e) {
                Toast.makeText(this, "Se ha producido un error inesperado.", Toast.LENGTH_SHORT);
            }
        }
        entrada.setText("");
        operacion=2;

    }

    public void multi(View v){

        if (num1_ocup==false) {
            try {

                if (porcent == false) {

                    String aux = entrada.getText().toString();
                    num1 = Double.parseDouble(aux);
                    punto = false;
                    num1_ocup=true;

                }

            } catch (NumberFormatException nfe) {
            }
        }
        else {
            try {
                String aux1 = entrada.getText().toString();
                num2 = Double.parseDouble(aux1);

                if (porcent == true){

                    num2=num1*(num2/100);

                }

                switch (operacion) {

                    case 1:
                        num1 = num1 + num2;
                        punto=false;
                        break;
                    case 2:
                        num1 = num1 - num2;
                        punto=false;
                        break;
                    case 3:
                        num1 = num1 * num2;
                        punto=false;
                        break;
                    case 4:
                        if (num2==Double.NaN) {
                            entrada.setText("Error");
                        }
                        else{
                            resultado=num1/num2;
                            punto=false;
                        }
                        break;

                }
            } catch (Exception e) {
                Toast.makeText(this, "Se ha producido un error inesperado.", Toast.LENGTH_SHORT);
            }
        }
        entrada.setText("");
        operacion=3;

    }

    public void div(View v){

        if (num1_ocup==false) {
            try {

                if (porcent == false) {
                    String aux = entrada.getText().toString();
                    num1 = Double.parseDouble(aux);
                    punto = false;
                    num1_ocup = true;
                }

            } catch (NumberFormatException nfe) {
            }
        }
        else {
            try {
                String aux1 = entrada.getText().toString();
                num2 = Double.parseDouble(aux1);

                if (porcent == true){

                    num2=num1*(num2/100);

                }

                switch (operacion) {

                    case 1:
                        num1 = num1 + num2;
                        punto=false;
                        break;
                    case 2:
                        num1 = num1 - num2;
                        punto=false;
                        break;
                    case 3:
                        num1 = num1 * num2;
                        punto=false;
                        break;
                    case 4:
                        if (num2==Double.NaN) {
                            entrada.setText("Error");
                        }
                        else{
                            resultado=num1/num2;
                            punto=false;

                        }
                        break;

                }
            } catch (Exception e) {
                Toast.makeText(this, "Se ha producido un error inesperado.", Toast.LENGTH_SHORT).show();
            }
        }
        entrada.setText("");
        operacion=4;

    }

    public void porcentaje(View v){

        if (porcent == false) {
            porcent = true;
        }
        else if (porcent == true) {
            porcent = false;
        }
    }

    public void exponencial(View v){


        if (num1_ocup == false) {
            try {

                if (porcent == false) {

                    String aux = entrada.getText().toString();
                    num1 = Double.parseDouble(aux);
                    punto = false;
                    num1_ocup=true;

                }
            } catch (NumberFormatException nfe) {
            }
        }
        else {
            try {
                String aux2 = entrada.getText().toString();
                num2 = Double.parseDouble(aux2);

                if (porcent == true){

                    num2=num1*(num2/100);

                }

                switch (operacion) {

                    case 1:
                        num1 = num1 + num2;
                        break;
                    case 2:
                        num1 = num1 - num2;
                        break;
                    case 3:
                        num1 = num1 * num2;
                        break;
                    case 4:
                        if (num2==Double.NaN) {
                            entrada.setText("Error");
                        }
                        else{
                            resultado=num1/num2;

                        }
                        break;
                    case 5:
                        num1= num1 * Math.pow(10 , num2);
                        break;

                }
            } catch (Exception e) {
                Toast.makeText(this, "Se ha producido un error inesperado.", Toast.LENGTH_SHORT).show();
            }
        }
        entrada.setText("");
        operacion = 5;

    }

    public void igual(View v){

        try{

            String aux1=entrada.getText().toString();
            num2=Double.parseDouble(aux1);
            punto=false;
            num1_ocup=false;

            if (porcent == true && operacion != 3){

                num2=num1*(num2/100);

            }
            if(porcent == true && operacion == 3){

                num2=num2/100;

            }

        }catch(NumberFormatException nfe){ }

        if (entrada.getText().toString().equalsIgnoreCase("") || entrada.getText().toString().equalsIgnoreCase("0.0")) {
            entrada.setText("");
        }
        else {
            entrada.setText("");

            if (operacion == 1){
                resultado= num1+num2;
                punto=false;
            }

            else if (operacion == 2){
                resultado= num1-num2;
                punto=false;
            }

            else if (operacion == 3){
                resultado= num1*num2;
                punto=false;
            }

            else if (operacion == 4){

                if (num2==Double.NaN) {
                    entrada.setText("Error");
                }
                else {
                    resultado = num1 / num2;
                }
                punto=false;
            }
            else if (operacion == 5){

                resultado= num1 * Math.pow(10 , num2);

            }

            entrada.setText(""+resultado);
            num1=resultado;
            porcent = false;
        }

    }
}
